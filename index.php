<?php
session_start();
if(!empty($_SESSION['loggedin']))
{
	$url = 'loggedin.php';
	header( "Location: $url" );
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome</title>
    </head>
    <body>
<form action="login.php" method="POST">
  <fieldset>
    <legend>Already registered? Login below</legend>
    Email:<br>
    <input type="email" name="email" required>
    <br>
    Password:<br>
    <input type="password" name="password" required>
    <br><br>
    <input type="submit" value="Login">
  </fieldset>
</form>
<form action="register.php" method="POST">
  <fieldset>
    <legend>Not a User? Register now!</legend>
	Name:<br>
    <input type="text" name="name" required>
    <br>
    Email:<br>
    <input type="email" name="email" required>
    <br>
    Password:<br>
    <input type="password" name="password" required>
	<br>
    Phone Number:<br>
    <input type="text" name="phoneNumber" required>
    <br><br>
    <input type="submit" value="Register">
  </fieldset>
</form>
	</body>
</html>